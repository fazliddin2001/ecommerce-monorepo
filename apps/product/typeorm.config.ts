module.exports = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'fazliddin2001',
  database: 'eproduct',
  synchronize: true,
  logging: true,
  entities: ['dist/**/*.entity.ts'],
  migrations: ['dist/migrations/**/*.js'],
  cli: {
    migrationsDir: 'src/migrations',
  },
};
