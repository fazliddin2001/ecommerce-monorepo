import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from './product.entity';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
  ) {}

  async getProducts(
    page = 1,
    pageSize = 10,
    sortBy?: string,
    sortOrder: 'ASC' | 'DESC' = 'ASC',
    productName?: string,
  ): Promise<Product[]> {
    try {
      console.log(productName);
      const queryBuilder = this.productRepository.createQueryBuilder('product');

      if (sortBy) {
        const validSortFields = ['productName', 'price', 'userID'];
        if (!validSortFields.includes(sortBy)) {
          throw new Error(`Invalid sortBy field: ${sortBy}`);
        }

        const order = sortOrder === 'ASC' ? 'ASC' : 'DESC';
        queryBuilder.addOrderBy(`product.${sortBy}`, order);
      }

      if (productName) {
        queryBuilder.andWhere('product.productName LIKE :name', {
          name: `%${productName}%`,
        });
      }

      const startIndex = (page - 1) * pageSize;
      const endIndex = page * pageSize;
      queryBuilder.skip(startIndex).take(endIndex - startIndex);

      const products = await queryBuilder.getMany();

      return products;
    } catch (error) {
      throw new Error('Failed to fetch products');
    }
  }

  async createProduct(payload: Product): Promise<Product> {
    try {
      console.log(payload);
      const newProduct = this.productRepository.save(payload);
      return newProduct;
    } catch (error) {
      throw new Error('Failed to create product');
    }
  }
}
