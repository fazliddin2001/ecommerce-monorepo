import { Controller } from '@nestjs/common';
import { ProductService } from './product.service';
import { MessagePattern } from '@nestjs/microservices';
import { Product } from './product.entity';

@Controller()
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @MessagePattern('productList')
  async getProducts(req: any): Promise<Product[]> {
    try {
      const { page = 1, pageSize = 10, sortBy, sortOrder, productName } = req;
      return await this.productService.getProducts(
        page,
        pageSize,
        sortBy,
        sortOrder,
        productName,
      );
    } catch (error) {
      throw new Error('Failed to fetch products');
    }
  }

  @MessagePattern('createProduct')
  async createProduct(payload: Product): Promise<Product> {
    try {
      console.log(payload);
      return await this.productService.createProduct(payload);
    } catch (error) {
      throw new Error('Failed to create product');
    }
  }
}
