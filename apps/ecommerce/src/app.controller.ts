import { Body, Controller, Get, Post, Query, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { User } from './dtos/user.dto';
import { CreateProduct, GetProductReq } from './dtos/product.dto';
import { RolesGuard } from '@app/auth/roles.guard';
import { Roles } from '@app/auth/roles.decorator';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('user/login')
  async login(@Body() user: User): Promise<string> {
    const result = await this.appService.login(user);
    return result;
  }

  @Post('user/register')
  async register(@Body() user: User): Promise<string> {
    const result = await this.appService.register(user);
    return result;
  }

  @Get('user/all')
  @UseGuards(RolesGuard)
  @Roles('admin')
  async getUsers(): Promise<string> {
    const result = await this.appService.getUsers();
    return result;
  }

  @Get('product/all')
  @UseGuards(RolesGuard)
  @Roles('user', 'admin')
  async getProducts(@Query() req: GetProductReq): Promise<string> {
    const result = await this.appService.getProducts(req);
    return result;
  }

  @Post('product/create')
  @UseGuards(RolesGuard)
  @Roles('admin')
  async createProduct(@Body() payload: CreateProduct): Promise<string> {
    const result = await this.appService.createProduct(payload);
    return result;
  }
}
