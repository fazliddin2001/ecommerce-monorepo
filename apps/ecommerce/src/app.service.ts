import { Injectable } from '@nestjs/common';
import {
  ClientProxyFactory,
  Transport,
  ClientProxy,
} from '@nestjs/microservices';
import { User } from './dtos/user.dto';
import { CreateProduct, GetProductReq } from './dtos/product.dto';

@Injectable()
export class AppService {
  private user: ClientProxy;
  private product: ClientProxy;

  constructor() {
    this.user = ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        port: 4000,
      },
    });

    this.product = ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        port: 5000,
      },
    });
  }

  public async login(user: User): Promise<string> {
    try {
      const result = await this.user
        .send<string, User>('login', user)
        .toPromise();

      return result;
    } catch (error) {
      throw new Error('Login failed');
    }
  }

  public async register(user: User): Promise<string> {
    try {
      const result = await this.user
        .send<string, User>('register', user)
        .toPromise();

      return result;
    } catch (error) {
      throw new Error('Failed to register');
    }
  }

  public async getUsers(): Promise<string> {
    try {
      const result = await this.user
        .send<string, object>('all', {})
        .toPromise();

      return result;
    } catch (error) {
      throw new Error('Failed to get all users');
    }
  }

  public getProducts(req: GetProductReq): Promise<string> {
    return this.product
      .send<string, GetProductReq>('productList', req)
      .toPromise();
  }

  public createProduct(payload: CreateProduct): Promise<string> {
    return this.product
      .send<string, CreateProduct>('createProduct', payload)
      .toPromise();
  }
}
