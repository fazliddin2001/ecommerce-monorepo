export class User {
  email: string;
  role: string;
  createdAt: Date;
  updatedAt: Date;
}
