export class GetProductReq {
  page: string;
  pageSize: string;
  productName: string;
  sortBy: string;
  sortOrder: string;
}

export class CreateProduct {
  productName: string;
  price: number;
  userID: number;
  createdAt: Date;
  updatedAt: Date;
}
