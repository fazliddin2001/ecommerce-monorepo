export class UserDTO {
  userID: number;
  email: string;
  role: string;
  createdAt: Date;
  updatedAt: Date;
  token: {
    jwt: string;
  };
}
