import { Injectable, UnauthorizedException } from '@nestjs/common';
import { User } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { AuthService } from '@app/auth';
import { UserDTO } from './dtos/user.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private readonly authService: AuthService,
  ) {}

  async login(user: User): Promise<UserDTO> {
    const result = await this.userRepository.findOne({
      where: { email: user.email },
    });

    if (result && (await bcrypt.compare(user.password, result.password))) {
      const jwt = this.authService.generateToken(result.userID, result.role);
      const userDTO: UserDTO = {
        ...result,
        token: {
          jwt,
        },
      };
      return userDTO;
    }

    throw new UnauthorizedException('Invalid credentials');
  }

  async register(user: User): Promise<User> {
    const existingUser = await this.userRepository.findOne({
      where: { email: user.email },
    });

    if (existingUser) {
      throw new Error('User with this email already exists');
    }

    const hashedPassword = await bcrypt.hash(user.password, 10);
    const newUser = this.userRepository.save({
      ...user,
      password: hashedPassword,
    });

    return newUser;
  }

  async getUsers(): Promise<User[]> {
    const result = await this.userRepository.find({});
    return result;
  }
}
