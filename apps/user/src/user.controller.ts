import { Controller } from '@nestjs/common';
import { UserService } from './user.service';
import { MessagePattern } from '@nestjs/microservices';
import { User } from './user.entity';
import { UserDTO } from './dtos/user.dto';

@Controller()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @MessagePattern('login')
  async login(user: User): Promise<UserDTO> {
    return this.userService.login(user);
  }

  @MessagePattern('register')
  async register(user: User): Promise<User> {
    return this.userService.register(user);
  }

  @MessagePattern('all')
  async getUsers(): Promise<User[]> {
    return this.userService.getUsers();
  }
}
