// libs/auth/src/auth.module.ts
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { authConfig } from '../auth.config';

@Module({
  imports: [
    JwtModule.register({
      secret: authConfig.secret,
    }),
  ],
  providers: [AuthService],
  exports: [AuthService],
})
export class AuthModule {}
