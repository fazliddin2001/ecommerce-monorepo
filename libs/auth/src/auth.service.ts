import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { authConfig } from '../auth.config';

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {}

  generateToken(userID: number, userRole: string): string {
    return this.jwtService.sign(
      { userID, role: userRole },
      { secret: authConfig.secret },
    );
  }

  async validateToken(token: string, roles: string[]): Promise<boolean> {
    try {
      const decoded = this.jwtService.verify(token);
      console.log(decoded);
      const userRole: string = decoded.role;

      const hasRole = roles.includes(userRole);
      if (!hasRole) {
        console.log(true);
        console.log(userRole);
        throw new UnauthorizedException('Insufficient permissions');
      }

      return true;
    } catch (error) {
      throw new UnauthorizedException('Invalid token');
    }
  }
}
